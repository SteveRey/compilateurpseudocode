import java.util.Arrays;
import java.util.ArrayList;

public class ArbreBNF{
    // chaque objet contient 5 variables d'instances :
    private ArbreBNF pere, premierFils, frereDroit;                 // references vers pere du noeud courant, fils gauche et droit
    private String type, valeur;                                    // type du noeud courant (la clef) et sa valeur associee (la donnee)

    // structure de l'abre :
    public ArbreBNF(ArbreBNF p, ArbreBNF fG, ArbreBNF fD, String pType, String pValeur){
        pere        = p;
        premierFils = fG;
        frereDroit  = fD;
        type        = pType;            // exemples de type : <nombre>, <declaration>, <type>, <chaine de caracteres>, etc.
        valeur      = pValeur;          // exemple de valeur : ENTIER (pour le type <type>)
    }

    // methode recursive en charge de la construction de l'arbre BNF
    public boolean constructionArbreBNF(){
        // on evalue le type du noeud courant et on realise l'action appropriee selon le tye

        if(type.equals("<programme>")){                                 //fils 1 = id. du progr., fils 2 = corps du progr.
            String[] valeurs = valeur.split(" ");                       //on decoupe la chaine valeur selon les " "
            if(!(valeurs[0].equals("PROGRAMME") && valeurs.length > 2))
                return false;                                           //on renvoie faux si la regle de reecriture n'est pas respectee
            premierFils = new ArbreBNF(this, null, null, "<id>", valeurs[1]);
            if(!premierFils.constructionArbreBNF())
                return false;                                           //on renvoie faux si le 1er fils n'est pas de type <id>
            String corps = new String(valeurs[2]);
            for(int i = 3; i < valeurs.length; i++)                     //corps = les autres morceaux de valeur
                corps = new String(corps + " "+valeurs[i]);
            premierFils.frereDroit = new ArbreBNF(this, null, null, "<corps programme>", corps);
            if(!premierFils.frereDroit.constructionArbreBNF())
                return false;                                           //on renvoie faux si le reste n'est pas de type <corps programme>

        }
        else if(type.equals("<corps programme>")){
            String[] valeurs = valeur.split(" ");
            int i = 0, indiceDebutMain = 0;
            boolean debutMainPresent = false, finMainPresent = false;
            while(i < valeurs.length){
                if(valeurs[i].equals("DEBUT_MAIN")){
                    debutMainPresent = true;
                    indiceDebutMain = i;
                }
                if(valeurs[i].equals("FIN_MAIN"))
                    finMainPresent = true;
                i++;
            }
            if(!(debutMainPresent && finMainPresent))           // si DEBUT_MAIN et FIN_MAIN non presents on arrete (erreur de syntaxe)
                return false;

            String declarationMethodes = new String();         // string liste declarations methodes
            for(int j = 0; j < indiceDebutMain; j++)
                declarationMethodes = new String(declarationMethodes+" "+valeurs[j]);
            
            // implementation de <liste instruction>
            String corpsProgramme = new String();                   // string corps du programme (entre debut et fin main)
            for(int j = indiceDebutMain+1; j < valeurs.length-1; j++)
                corpsProgramme = new String(corpsProgramme+" "+valeurs[j]);
            if(!declarationMethodes.isEmpty()){
            premierFils = new ArbreBNF(this, null, null, "<liste declarations methodes>", declarationMethodes);
            if(!premierFils.constructionArbreBNF())
                return false;                                       // on renvoie faux si le 1er fils n'est pas de type <liste declarations methodes>
        
            premierFils.frereDroit = new ArbreBNF(this, null, null, "<liste instructions>", corpsProgramme);
            if(!premierFils.frereDroit.constructionArbreBNF())
                return false;                                       // on renvoie faux si le 2e fils n'est pas de type <liste instructions>
            }else{
                premierFils = new ArbreBNF(this, null, null, "<liste instructions>", corpsProgramme);
            if(!premierFils.constructionArbreBNF())
                return false;                                       // on renvoie faux si le premier fils n'est pas de type <liste instructions>
            }


        }
        else if(type.equals("<id>")){
            // on verifie que seul les caracteres autorises sont presents
            for(int i = 0; i < valeur.length(); i++){
                if(i == 0){
                    if(! ((valeur.charAt(0) >= 'a' && valeur.charAt(0) <= 'z') ||
                         (valeur.charAt(0) >= 'A' && valeur.charAt(0) <= 'Z')))
                            return false;
                }else{
                    if(! ((valeur.charAt(i) >= '0' && valeur.charAt(i) <= '9') ||
                         (valeur.charAt(i) >= 'a' && valeur.charAt(i)  <= 'z') ||
                         (valeur.charAt(i) >= 'A' && valeur.charAt(i)  <= 'Z')))
                            return false;
                }
            }
        }
        else if(type.equals("<chaine de caracteres>")){
            for(int i = 0; i < valeur.length(); i++){
                // on verifie que seul les caracteres autorises suivant sont presents dans la chaine
                if(! ((valeur.charAt(i) >= '0' && valeur.charAt(i) <= '9')   || (valeur.charAt(i) >= 'a' && valeur.charAt(i) <= 'z')    ||
                      (valeur.charAt(i) >= 'A' && valeur.charAt(i) <= 'Z')   || (valeur.charAt(i) == ',') || (valeur.charAt(i) == '.')  ||
                      (valeur.charAt(i) == ':') || (valeur.charAt(i) == ';') || (valeur.charAt(i) == '?') || (valeur.charAt(i) == '!')  ||
                      (valeur.charAt(i) == ' ') || (valeur.charAt(i) == '[') || (valeur.charAt(i) == ']') || (valeur.charAt(i) == '\'') ||
                      (valeur.charAt(i) == '+') || (valeur.charAt(i) == '-') || (valeur.charAt(i) == '/') || (valeur.charAt(i) == '*')  ||
                      (valeur.charAt(i) == '#') || (valeur.charAt(i) == '=') || (valeur.charAt(i) == '<') || (valeur.charAt(i) == '>')  ||
                      (valeur.charAt(i) == '(') || (valeur.charAt(i) == ')') ))
                        return false;
            }
        }
        else if(type.equals("<type>")){
            // on verifie que seul les mots autorises sont presents
            if(! (valeur.equals("ENTIER") || valeur.equals("CHAINE") || valeur.equals("TABLEAU") || valeur.equals("BOOLEEN")) )
                return false;
        }
        else if(type.equals("<nombre>")){
            // on verifie que seul les chiffres autorises sont presents
            for(int i = 0; i < valeur.length(); i++){
                if(!(valeur.charAt(i) >= '0' && valeur.charAt(i) <= '9'))
                    return false;
            }
        }
        else if(type.equals("<declaration>")){
            String[] valeurs = valeur.split(":");                                             // on decoupe la chaine selon ":"
            premierFils = new ArbreBNF(this, null, null, "<id>", valeurs[0]);
                if(!premierFils.constructionArbreBNF())
                    return false;                                                                  // on renvoie faux si le premier fils n'est pas de type <id>
            premierFils.frereDroit  = new ArbreBNF(this, null, null, "<type>", valeurs[1]);
                if(!premierFils.frereDroit.constructionArbreBNF())
                    return false;                                                                 // on renvoie faux si le 2e fils n'est pas de type <type>
        }
        else if(type.equals("<affectation chaine>")){
            String[] valeurs = valeur.split(":=");
            premierFils = new ArbreBNF(this, null, null, "<id>", valeurs[0]);
            if(!premierFils.constructionArbreBNF())
                return false;                                                                     // faux si 1er fils pas de type <id>
            if(valeurs[1].charAt(0) != '"' || valeurs[1].charAt(valeurs[1].length()-1) != '"')    // faux si " pas presentes en debut et fin chaine
                return false;

            String[] chaineCaract = valeurs[1].split("\"");
            String caracteres = "";
            for(int i = 0; i < chaineCaract.length; i++)
                caracteres = caracteres + chaineCaract[i];

            premierFils.frereDroit = new ArbreBNF(this, null, null, "<chaine de caracteres>", caracteres);
            if(!premierFils.frereDroit.constructionArbreBNF()) {
                return false;                                                                     // on renvoie faux si le 2e fils n'est pas de type <chaine de caracteres>
            }
        }
        else if(type.equals("<liste instructions>")){

            // construit tableau instructions (supprime lignes vides, la fonction est tout a la fin):
            String[] listeInstructions = constructionListeInstructions(valeur);

            String[] temp = new String[2];
            temp[0] = listeInstructions[0].trim();          // enleve les espaces au debut
            temp[1] = "";

            for(int i = 1; i < listeInstructions.length; i++)
                temp[1] += listeInstructions[i];
            listeInstructions = temp;

            // extrait le premier mot pour savoir si boucle while, do ou for
            String[] premierMot = listeInstructions[0].split(" ");

            if ((premierMot[0].contains("POUR"))){
                premierFils = new ArbreBNF(this, null, null, "<boucle pour>", listeInstructions[0]);
                if(!premierFils.constructionArbreBNF())
                    return false;                                                               // on renvoie faux si le premier fils n'est pas de type <boucle pour>
                if(!listeInstructions[1].isEmpty()){
                    premierFils.frereDroit = new ArbreBNF(this, null, null, "<liste instructions>", listeInstructions[1]);
                    if(!premierFils.frereDroit.constructionArbreBNF())
                        return false;                                                          // on renvoie faux si le 2e fils n'est pas de type <liste instructions>
                }

            }
            else if ((premierMot[0].contains("SI") && !premierMot[0].contains("SIR"))){       // car le mot "SAISIR" contient le substring "SI"
                premierFils = new ArbreBNF(this, null, null, "<bloc si>", listeInstructions[0]);
                if(!premierFils.constructionArbreBNF()) {
                    return false;                                                             // on renvoie faux si le premier fils n'est pas de type <bloc si>
                }
                if(!listeInstructions[1].isEmpty()){
                    premierFils.frereDroit = new ArbreBNF(this, null, null, "<liste instructions>", listeInstructions[1]);
                    if(!premierFils.frereDroit.constructionArbreBNF()) {
                        return false;                                                        // on renvoie faux si le 2e fils n'est pas de type <liste instructions>
                    }
                }
            }
            else if ((premierMot[0].contains("TANT_QUE"))){
                premierFils = new ArbreBNF(this, null, null, "<boucle tant que>", listeInstructions[0]);
                if(!premierFils.constructionArbreBNF())
                    return false;                                                           // on renvoie faux si le premier fils n'est pas de type <boucle tant que>
                if(!listeInstructions[1].isEmpty()){
                    premierFils.frereDroit = new ArbreBNF(this, null, null, "<liste instructions>", listeInstructions[1]);
                    if(!premierFils.frereDroit.constructionArbreBNF())
                        return false;                                                       // on renvoie faux si le 2e fils n'est pas de type <liste instructions>
                }
            }
            else if(listeInstructions[0].contains(":=\"")){                        // construit <affectation chaine>
                premierFils = new ArbreBNF(this, null, null, "<affectation chaine>", listeInstructions[0]);
                if(!premierFils.constructionArbreBNF())
                    return false;

                if(!listeInstructions[1].isEmpty()){                               // construit <liste instrcutions>
                    premierFils.frereDroit = new ArbreBNF(this, null, null, "<liste instructions>", listeInstructions[1]);
                    if(!premierFils.frereDroit.constructionArbreBNF())
                        return false;
                }
            }
            else if(listeInstructions[0].contains(":=")){                         // construit <affectation>
                premierFils = new ArbreBNF(this, null, null, "<affectation>", listeInstructions[0]);
                if(!premierFils.constructionArbreBNF())
                    return false;
                if(!listeInstructions[1].isEmpty()){
                    premierFils.frereDroit = new ArbreBNF(this, null, null, "<liste instructions>", listeInstructions[1]);
                    if(!premierFils.frereDroit.constructionArbreBNF())
                        return false;
                }

            }
            else if(listeInstructions[0].contains(":")){                         // construit <declaration>
                premierFils = new ArbreBNF(this, null, null, "<declaration>", listeInstructions[0]);
                if(!premierFils.constructionArbreBNF())
                    return false; 
                if(!listeInstructions[1].isEmpty()){
                    premierFils.frereDroit = new ArbreBNF(this, null, null, "<liste instructions>", listeInstructions[1]);
                    if(!premierFils.frereDroit.constructionArbreBNF())
                        return false; 
                }
            }
            else{                                                   // si ce n'est pas les autres alors c'est un <appel>
                premierFils = new ArbreBNF(this, null, null, "<appel>", listeInstructions[0]);
                if(!premierFils.constructionArbreBNF()) {
                    return false;
                }
                if(!listeInstructions[1].isEmpty()){
                    premierFils.frereDroit = new ArbreBNF(this, null, null, "<liste instructions>", listeInstructions[1]);
                    if(!premierFils.frereDroit.constructionArbreBNF())
                        return false;
                }
            }
        }
        else if(type.equals("<terme>")){
            String temp = new String(valeur);               // pour ne pas alterer la chaine originale
            String[] appelOuNombre = temp.split("\\(");
            if(appelOuNombre[0].equals("APPELER")){
                premierFils = new ArbreBNF(this, null, null, "<appel>", temp);
                if(!premierFils.constructionArbreBNF())
                    return false; 
            }else{
                if(valeur.charAt(0) >= '0' && valeur.charAt(0) <= '9'){
                    premierFils = new ArbreBNF(this, null, null, "<nombre>", valeur);
                    if(!premierFils.constructionArbreBNF())
                        return false;
                }else if(valeur.contains("[")){
                    String[] valeurs = valeur.split("\\[");
                    premierFils = new ArbreBNF(this, null, null, "<id>", valeurs[0]);
                    if(!premierFils.constructionArbreBNF())
                        return false; 
                    String valeurSansCrochetFermant = valeurs[1].replace("]", "");
                    premierFils.frereDroit = new ArbreBNF(this, null, null, "<id>", valeurSansCrochetFermant);
                    if(!premierFils.frereDroit.constructionArbreBNF())
                        return false; 
                }else if(valeur.equals("VRAI") || valeur.equals("FAUX")){
                    premierFils = new ArbreBNF(this, null, null, "<id>", valeur);
                    if(!premierFils.constructionArbreBNF())
                        return false; 
                }else{
                    premierFils = new ArbreBNF(this, null, null, "<id>", valeur);
                    if(!premierFils.constructionArbreBNF())
                        return false; 
                }
            }
        }
        else if(type.equals("<expression>")){
            int nbParentheseOuverture = 0;
            int nbParentheseFermeture = 0;
            if(valeur.length() == 1){
                premierFils = new ArbreBNF(this, null, null, "<terme>", valeur);
                if(!premierFils.constructionArbreBNF()){
                    return false;
                }
                
            }
            if(valeur.contains("+") || valeur.contains("-") || valeur.contains("/") || valeur.contains("*")){
                for(int i = 0; i < valeur.length(); i++){
                    if(valeur.charAt(i) == '(')
                        nbParentheseOuverture++;
                    else if(valeur.charAt(i) == ')')
                        nbParentheseFermeture++;
                    else if(valeur.charAt(i) == '+' || valeur.charAt(i) == '-' || valeur.charAt(i) == '/' || valeur.charAt(i) == '*'){
                        if(nbParentheseOuverture == nbParentheseFermeture){
                            String expressionGauche = valeur.substring(0, i);
                            String operateur = String.valueOf(valeur.charAt(i));
                            String expressionDroite = valeur.substring(i+1);
                            premierFils = new ArbreBNF(this, null, null, "<terme>", expressionGauche);
                            if(!premierFils.constructionArbreBNF())
                                return false;

                            premierFils.frereDroit = new ArbreBNF(this, null, null, "<chaine de caracteres>", operateur);
                            if(!premierFils.frereDroit.constructionArbreBNF())
                                return false;

                            premierFils.frereDroit.frereDroit = new ArbreBNF(this, null, null, "<terme>", expressionDroite);
                            if(!premierFils.frereDroit.frereDroit.constructionArbreBNF())
                                return false;
                        }else
                            return false;
                    }
                }
            }else{
                premierFils = new ArbreBNF(this, null, null, "<terme>", valeur);
                if(!premierFils.constructionArbreBNF()) 
                    return false;

            }
        }
        else if(type.equals("<affectation>")){
            String[] valeurs = valeur.split(":=");              // on decoupe la chaine selon :=
            if(!valeurs[0].contains("[")){
                premierFils = new ArbreBNF(this, null, null, "<id>", valeurs[0]);
                if(!premierFils.constructionArbreBNF())
                    return false;
            }else{                          // si le mot contient [ alors on a a faire a un tableau
                String[] idGauche = valeurs[0].split("\\[");
                String[] idDroit  = idGauche[1].split("\\]");

                premierFils = new ArbreBNF(this, null, null, "<id>", idGauche[0]);
                if(!premierFils.constructionArbreBNF())
                    return false;

                premierFils.frereDroit = new ArbreBNF(this, null, null, "<id>", idDroit[0]);
                if(!premierFils.frereDroit.constructionArbreBNF())
                    return false;
            }
            // on fait la partie <expression>
            if(premierFils.frereDroit == null){
                premierFils.frereDroit = new ArbreBNF(this, null, null, "<expression>", valeurs[1]);
                if(!premierFils.frereDroit.constructionArbreBNF())
                    return false;
            }else{
                premierFils.frereDroit.frereDroit = new ArbreBNF(this, null, null, "<expression>", valeurs[1]);
                if(!premierFils.frereDroit.frereDroit.constructionArbreBNF())
                    return false;
            }
            
        }
        else if(type.equals("<liste parametres>")){
            String[] valeurs = valeur.split(",");                    // on decoupe la chaine selon ,

            int i = 0;
            String[] listeParam = valeurs[i].split(":");
            premierFils = new ArbreBNF(this, null, null, "<id>", listeParam[0]);
                if(!premierFils.constructionArbreBNF())
                    return false;

            ArbreBNF filsCourant = premierFils;
            int j = 1;
            while(filsCourant != null && i < valeurs.length){
                listeParam = valeurs[i].split(":");
                if (j == 0){
                    filsCourant.frereDroit = new ArbreBNF(this, null, null, "<id>", listeParam[j]);
                    if(!premierFils.constructionArbreBNF())
                        return false;   
                    j = 1;
                }else{
                    filsCourant.frereDroit = new ArbreBNF(this, null, null, "<type>", listeParam[j]);
                    if(!filsCourant.constructionArbreBNF())
                        return false;
                    j = 0;
                }
                if(j == 0)
                    i++;
                filsCourant = filsCourant.frereDroit;
            }
            

        }
        else if(type.equals("<liste declarations methodes>")){
            // -------------------- creation des variables --------------------- //
            String[] valeurs = valeur.split(" ");


            int compteurDebut = 0, compteurFin = 0, indiceDebut = -1, indiceFin = -1;
            for(int i = 0; i < valeurs.length; i++){
                if(valeurs[i].equals("DEBUT")){
                    compteurDebut++;
                    if(indiceDebut == -1)
                        indiceDebut = i;
                }
                if(valeurs[i].equals("FIN")){
                    compteurFin++;
                    if(indiceFin == -1)
                        indiceFin = i;
                }
            }
            if(compteurDebut != compteurFin || indiceDebut == indiceFin || indiceDebut > indiceFin || indiceFin == indiceDebut+1) {
                return false;
            }

            String listeInstructions = "";
            for(int i = indiceDebut+1; i < indiceFin; i++)
                listeInstructions = listeInstructions + " " + valeurs[i];

            String[] id = valeurs[indiceDebut-1].split("\\(");
            String[] listeParamEtDeclarationMethode = id[1].split("\\):");
            String[] listeDeclaMethode = valeur.split("FIN\\b");            // \b (regex) sert a avoir le pattern exact (evite les FIN_SI etc)

            // sert a remettre le FIN si on a plusieurs methodes car la ligne precedente supprime tous les "FIN"
            for (int i = 1; i < listeDeclaMethode.length; i++)
                listeDeclaMethode[i] = listeDeclaMethode[i] + "FIN";
            // -------------------- fin creation variables --------------------- //

            // -------------------- creation de l'arbre BNF -------------------- //
            premierFils = new ArbreBNF(this, null, null, "<id>", id[0]);
            if(!premierFils.constructionArbreBNF())
                return false;

            premierFils.frereDroit = new ArbreBNF(this, null, null, "<liste parametres>", listeParamEtDeclarationMethode[0]);
            if(!premierFils.frereDroit.constructionArbreBNF())
                return false;

            if(!listeParamEtDeclarationMethode[1].equals("RIEN"))
                premierFils.frereDroit.frereDroit = new ArbreBNF(this, null, null, "<type>", listeParamEtDeclarationMethode[1]);
            else
                premierFils.frereDroit.frereDroit = new ArbreBNF(this, null, null, "<id>", "RIEN");
            if(!premierFils.frereDroit.frereDroit.constructionArbreBNF())
                return false;

            premierFils.frereDroit.frereDroit.frereDroit = new ArbreBNF(this, null, null, "<liste instructions>", listeInstructions);
            if(!premierFils.frereDroit.frereDroit.frereDroit.constructionArbreBNF())
                return false;

            if(listeDeclaMethode.length > 1){
                premierFils.frereDroit.frereDroit.frereDroit.frereDroit = new ArbreBNF(this, null, null, "<liste declarations methodes>", listeDeclaMethode[1]);
                if(!premierFils.frereDroit.frereDroit.frereDroit.frereDroit.constructionArbreBNF())
                    return false;
            }
        }
        else if(type.equals("<liste parametres appel>")){
            String[] valeurs = valeur.split(",");

            premierFils = new ArbreBNF(this, null, null, "<expression>", valeurs[0]);
            if(!premierFils.constructionArbreBNF())
                return false;

            if(valeurs.length > 1){
                String paramRestants = "";
                for(int i = 1; i < valeurs.length; i++){
                    if(i < valeurs.length-1)
                        paramRestants = paramRestants + valeurs[i]+",";
                    else
                        paramRestants = paramRestants + valeurs[i];

                }
                premierFils.frereDroit = new ArbreBNF(this, null, null, "<liste parametres appel>", paramRestants);
                if(!premierFils.frereDroit.constructionArbreBNF())
                    return false;
            }
        }
        else if(type.equals("<appel>")){
            String[] valeurs = valeur.split("\\(");

            premierFils = new ArbreBNF(this, null, null, "<feuille>", valeurs[0]);
                if(!premierFils.constructionArbreBNF())
                    return false;
            if(valeurs[0].equals("APPELER")){
                premierFils.frereDroit = new ArbreBNF(this, null, null, "<id>", valeurs[1]);
                if(!premierFils.frereDroit.constructionArbreBNF())
                    return false;


                String[] listeParamAppel = valeurs[2].split("\\)");
                premierFils.frereDroit.frereDroit = new ArbreBNF(this, null, null, "<liste parametres appel>", listeParamAppel[0]);
                if(!premierFils.frereDroit.frereDroit.constructionArbreBNF())
                    return false;

            }else if(valeurs[0].equals("SAISIR") || valeurs[0].equals("SAISIR_ENTIER")){
                String[] saisirId = valeurs[1].split("\\)");
                premierFils.frereDroit = new ArbreBNF(this, null, null, "<id>", saisirId[0]);
                if(!premierFils.frereDroit.constructionArbreBNF())
                    return false;
                
            }else if(valeurs[0].equals("RETOURNER") || valeurs[0].equals("AFFICHER") || valeurs[0].equals("AFFICHER_LIGNE")){
                String[] retourExpression = valeurs[1].split("\\)");
                premierFils.frereDroit = new ArbreBNF(this, null, null, "<expression>", retourExpression[0]);
                if(!premierFils.frereDroit.constructionArbreBNF())
                    return false;
                
            }else
                return false;
        }
        else if(type.equals("<boucle pour>")){
            String[] valeurs = valeur.split(" ");
            if(valeurs.length < 9)
                return false;           // la condition est mal ecrite

            if(!(valeurs[0].equals("POUR") && valeurs[2].equals("ALLANT") && valeurs[3].equals("DE") &&
                valeurs[5].equals("A") && valeurs[7].equals("FAIRE")))
                return false;


            premierFils = new ArbreBNF(this, null, null, "<id>", valeurs[1]);
            if(!premierFils.constructionArbreBNF())
                return false;
            premierFils.frereDroit = new ArbreBNF(this, null, null, "<expression>", valeurs[4]);
            if(!premierFils.frereDroit.constructionArbreBNF())
                return false;
            premierFils.frereDroit.frereDroit = new ArbreBNF(this, null, null, "<expression>", valeurs[6]);
            if(!premierFils.frereDroit.frereDroit.constructionArbreBNF())
                return false;

            String listeInstructions = "";
            for(int i = 8; i < valeurs.length-1; i++)
                listeInstructions = listeInstructions + " " + valeurs[i];

            
            premierFils.frereDroit.frereDroit.frereDroit = new ArbreBNF(this, null, null, "<liste instructions>", listeInstructions);
            if(!premierFils.frereDroit.frereDroit.frereDroit.constructionArbreBNF())
                return false;
        }
        else if(type.equals("<condition>")){
            String[] valeurs = valeur.split(" ");           // on decoupe selon les espaces
            String expression1 = "";
            String expression2 = "";

            int indiceDepart;                                   // calcul de l'indice de depat de la condition
            if(valeurs[0].isEmpty())
                indiceDepart = 1;
            else
                indiceDepart = 0;

            int i;
            if(valeurs[indiceDepart].charAt(0) == '(')
                i = 1;
            else
                i = 0;

            while(valeurs[indiceDepart].charAt(i) != '=' &&  valeurs[indiceDepart].charAt(i) != '#' && valeurs[indiceDepart].charAt(i) != '<' && valeurs[indiceDepart].charAt(i) != '>'){
                expression1 = expression1 +valeurs[indiceDepart].charAt(i);
                i++;
            }

            premierFils = new ArbreBNF(this, null, null, "<expression>", expression1);
            if(!premierFils.constructionArbreBNF())
                return false;

            premierFils.frereDroit = new ArbreBNF(this, null, null, "<feuille>", Character.toString(valeurs[indiceDepart].charAt(i)));
            if(!premierFils.frereDroit.constructionArbreBNF())
                return false;

            i++;
            while(i < valeurs[indiceDepart].length()){
                expression2 = expression2 + valeurs[indiceDepart].charAt(i);
                i++;
            }

            premierFils.frereDroit.frereDroit = new ArbreBNF(this, null, null, "<expression>", expression2);
            if(!premierFils.frereDroit.frereDroit.constructionArbreBNF())
                return false;

            if(valeurs.length > 1 && !valeurs[0].isEmpty()){
                if(!valeurs[1].equals("OU") || !valeurs[1].equals("ET"))
                    return false;

                premierFils.frereDroit.frereDroit.frereDroit = new ArbreBNF(this, null, null, "<feuille>", valeurs[1]);
                if(!premierFils.frereDroit.frereDroit.frereDroit.constructionArbreBNF())
                    return false;

                String[] condition = valeurs[2].split("\\)");

                premierFils.frereDroit.frereDroit.frereDroit.frereDroit = new ArbreBNF(this, null, null, "<condition>", condition[0]);
                if(!premierFils.frereDroit.frereDroit.frereDroit.frereDroit.constructionArbreBNF())
                    return false;
            }

        }
        else if(type.equals("<boucle tant que>") || type.equals("<bloc si>")){
            String[] valeurs = valeur.split(" ");
            if(!valeurs[0].equals("SIR") && valeurs[0].equals("SI")){       // car le mot "SAISIR" contient "SI"
                if(!valeurs[valeurs.length-1].equals("FIN_SI"))
                    return false;

                int indiceAlors = -1, compteurIndiceAlors = 0, compteurSi = 0, compteurFin_Si = 0;
                int parcours = 0;
                while(parcours < valeurs.length){
                    if (valeurs[parcours].equals("ALORS")) {
                        if(compteurIndiceAlors == 0)
                            indiceAlors = parcours;
                        compteurIndiceAlors++;
                    }
                    else if (valeurs[parcours].equals("SI"))
                        compteurSi++;
                    else if (valeurs[parcours].equals("FIN_SI"))
                        compteurFin_Si++;
                    parcours++;
                }

                if(compteurIndiceAlors != compteurSi && compteurSi != compteurFin_Si)
                    return false;

                if(indiceAlors == -1)           // si "ALORS" pas trouve
                    return false;


                String partieEntreSiAlors = "";
                for(int i = 1; i < indiceAlors; i++)
                    partieEntreSiAlors = partieEntreSiAlors + " "+ valeurs[i];
                premierFils = new ArbreBNF(this, null, null, "<condition>", partieEntreSiAlors);
                if(!premierFils.constructionArbreBNF())
                    return false;



                if(!valeur.contains("SINON")) {
                    String partieEntreAlorsEtFin = "";
                    for(int i = indiceAlors+1; i < valeurs.length-1; i++)
                        partieEntreAlorsEtFin = partieEntreAlorsEtFin + " "+valeurs[i];

                    premierFils.frereDroit = new ArbreBNF(this, null, null, "<liste instructions>", partieEntreAlorsEtFin);
                    if (!premierFils.frereDroit.constructionArbreBNF())
                        return false;
                }
                else {
                    // partie avant SINON :
                    int i = indiceAlors+1;
                    String avantSinon = "";
                    while(!valeurs[i].equals("SINON")){
                        avantSinon = avantSinon +" "+ valeurs[i];
                        i++;
                    }
                    i++;

                    premierFils.frereDroit = new ArbreBNF(this, null, null, "<liste instructions>", avantSinon);
                    if (!premierFils.frereDroit.constructionArbreBNF())
                        return false;

                    // partie apres SINON :
                    String sinon = "";
                    while(i < valeurs.length-1){
                        sinon = sinon + " "+valeurs[i];
                        i++;
                    }

                    premierFils.frereDroit.frereDroit = new ArbreBNF(this, null, null, "<liste instructions>", sinon);
                    if (!premierFils.frereDroit.frereDroit.constructionArbreBNF())
                        return false;
                }
            }
            else if(valeurs[0].equals("TANT_QUE")){
                if(!valeurs[valeurs.length-1].equals("FIN_TANT_QUE"))
                    return false;

                int indiceFaire = -1, compteurIndiceFaire = 0, compteurTant_Que = 0, compteurFin_Tant_Que = 0;
                int parcours = 0;
                while(parcours < valeurs.length){
                    if (valeurs[parcours].contains("FAIRE")) {
                        if(compteurIndiceFaire == 0)
                            indiceFaire = parcours;
                        compteurIndiceFaire++;
                    }
                    else if(valeurs[parcours].equals("TANT_QUE"))
                        compteurTant_Que++;
                    else if(valeurs[parcours].equals("FIN_TANT_QUE"))
                        compteurFin_Tant_Que++;
                    parcours++;
                }


                if(compteurIndiceFaire != compteurTant_Que && compteurTant_Que != compteurFin_Tant_Que)
                    return false;


                if(indiceFaire == -1)
                    return false;

                String partieEntreTant_QUEFaire = "";
                for(int i = 1; i < indiceFaire; i++)
                    partieEntreTant_QUEFaire = partieEntreTant_QUEFaire + " "+ valeurs[i];
                premierFils = new ArbreBNF(this, null, null, "<condition>", partieEntreTant_QUEFaire);
                if(!premierFils.constructionArbreBNF())
                    return false;

                String partieEntreFaireEtFin = "";
                for(int i = indiceFaire+1; i < valeurs.length-1; i++)
                    partieEntreFaireEtFin = partieEntreFaireEtFin + " "+valeurs[i];

                premierFils.frereDroit = new ArbreBNF(this, null, null, "<liste instructions>", partieEntreFaireEtFin);
                if(!premierFils.frereDroit.constructionArbreBNF())
                    return false;

            }
            else
                return false;
        }
        else if(type.equals("<feuille>")){
            // condition reservee a AFFICHER, AFFICHER_LIGNE, SAISIR, etc.
            if(valeur.length() == 1){
                if(!(valeur.equals("=") || valeur.equals("#") || valeur.equals("<") || valeur.equals(">") || valeur.equals("OU") || valeur.equals("ET")))
                    return false;
            }else{
                for(int i = 0; i < valeur.length(); i++){
                    if(i == 0){
                        if(! ((valeur.charAt(0) >= 'a' && valeur.charAt(0) <= 'z') ||
                            (valeur.charAt(0) >= 'A' && valeur.charAt(0) <= 'Z')))
                                return false;
                    }else{
                        if(! ((valeur.charAt(i) >= '0' && valeur.charAt(i) <= '9') ||
                            (valeur.charAt(i) >= 'a' && valeur.charAt(i)  <= 'z') ||
                            (valeur.charAt(i) >= 'A' && valeur.charAt(i)  <= 'Z') || 
                            (valeur.charAt(i) =='_'    )))
                                return false;
                    }
                }
            }
        }
        else  // si le noeud n'est pas reconnu : erreur de syntax --> la compilation ne peut se faire
            return false;
        
    return true;
    }

    // fonction qui "traduit" du code nfp136 en langage Java
    public String codeJava(){
        String ligne = "";
        if(type.equals("<programme>")){
            ligne = "import java.util.*;\n\npublic class " + premierFils.codeJava();
            ligne = ligne + " {\n\n" + premierFils.frereDroit.codeJava() + "}";
            return ligne;
        }
        else if(type.equals("<corps programme>")){
            if(premierFils.frereDroit != null){
                ligne = premierFils.codeJava();
                ligne = ligne + "public static void main(String[] args) {\n";
                ligne = ligne + premierFils.frereDroit.codeJava();
            }else{
                ligne = ligne + "public static void main(String[] args) {\n";
                ligne = ligne + premierFils.codeJava();
            }
            ligne = ligne + "}\n";
            return ligne;
        }
        else if(type.equals("<id>") || type.equals("<nombre>") || type.equals("<chaine de caracteres>")){
            return valeur;
        }
        else if(type.equals("<type>")){
            if(valeur.equals("ENTIER"))
                return "int";
            if(valeur.equals("CHAINE"))
                return "String";
            if(valeur.equals("TABLEAU"))
                return "int[]";
            if(valeur.equals("BOOLEEN"))
                return "boolean";
        }
        else if(type.equals("<declaration>")){
            ligne = premierFils.frereDroit.codeJava()+ " ";
            ligne = ligne + premierFils.codeJava();
            if(!premierFils.frereDroit.valeur.equals("TABLEAU"))
                ligne = ligne + ";\n";
            else
                ligne = ligne + " = new int[1000]" +";\n";
            return ligne;
        }
        else if(type.equals("<affectation chaine>")){
            ligne = premierFils.codeJava()+"=\"";
            ligne = ligne + premierFils.frereDroit.codeJava()+"\";\n";
            return ligne;
        }
        else if(type.equals("<liste instructions>")){
            ArbreBNF filsCourant = premierFils;
            while(filsCourant != null){
                ligne = ligne + filsCourant.codeJava();
                filsCourant = filsCourant.frereDroit;
            }

            return ligne;
        }
        else if(type.equals("<terme>")){
            if(premierFils != null && premierFils.frereDroit != null){
                ligne = ligne + premierFils.codeJava() + "[";
                if(premierFils.frereDroit != null)
                    ligne = ligne + premierFils.frereDroit.codeJava() + "]";
            }else{
                if(valeur.equals("VRAI"))
                    ligne = "true";
                else if (valeur.equals("FAUX"))
                    ligne = "false";
                else
                    ligne = ligne + premierFils.codeJava();
            }   
            return ligne;
        }
        else if(type.equals("<expression>")){
            if(premierFils.frereDroit == null){
                ligne = ligne + premierFils.codeJava();
            }else{
                ligne = ligne + premierFils.codeJava() + premierFils.frereDroit.valeur + premierFils.frereDroit.frereDroit.codeJava();
            }
            return ligne;
        }
        else if(type.equals("<affectation>")){
            if(premierFils.frereDroit.frereDroit == null){
                ligne = ligne + premierFils.codeJava() + "=" + premierFils.frereDroit.codeJava() + ";\n";
            }else{
                ligne = ligne + premierFils.codeJava() +"["+ premierFils.frereDroit.codeJava() +"]="+ premierFils.frereDroit.frereDroit.codeJava() +";\n";
            }
            return ligne;
        }
        else if(type.equals("<liste parametres>")){
            ArbreBNF filsCourant = premierFils;                     // sert a faire la boucle recursive
            int j = 0;                                              // sert a afficher correctement tous les 2 fils
            while(filsCourant.frereDroit != null){
                if(j%2 == 0){
                    ligne = ligne + filsCourant.frereDroit.codeJava();
                    ligne = ligne +" "+filsCourant.codeJava();
                    if(filsCourant.frereDroit.frereDroit != null)
                        ligne = ligne + ", ";
                }
                filsCourant = filsCourant.frereDroit;
                j++;
            }
        }
        else if(type.equals("<liste declarations methodes>")){
            if(premierFils != null){
                ligne = ligne + "public static ";
                if(premierFils.frereDroit.frereDroit != null && premierFils.frereDroit.frereDroit.valeur.equals("RIEN"))
                    ligne = ligne + "void ";
                else{
                    ligne = ligne + premierFils.frereDroit.frereDroit.codeJava();
                }

                ligne = ligne + " " + premierFils.codeJava() + "(" +premierFils.frereDroit.codeJava() + "){\n";
                if(premierFils.frereDroit.frereDroit.frereDroit != null)
                    ligne = ligne + premierFils.frereDroit.frereDroit.frereDroit.codeJava()+"}\n\n";
                if(premierFils.frereDroit.frereDroit.frereDroit.frereDroit != null)
                    ligne = ligne + premierFils.frereDroit.frereDroit.frereDroit.frereDroit.codeJava();
            }
            return ligne;
        }
        else if(type.equals("<liste parametres appel>")){
            ArbreBNF filsCourant = premierFils;
            while(filsCourant != null){
                ligne = ligne + filsCourant.codeJava();
                if(filsCourant.frereDroit != null)
                    ligne = ligne + ", ";
                filsCourant = filsCourant.frereDroit;
            }
            return ligne;
        }
        else if(type.equals("<appel>")){
            if(premierFils != null){
                if(premierFils.valeur.equals("APPELER")){
                    ligne = ligne + premierFils.frereDroit.codeJava()+"("+premierFils.frereDroit.frereDroit.codeJava()+")";
                    if(pere.type.equals("<liste instructions>"))
                        ligne = ligne + ";\n";
                }else if(premierFils.valeur.equals("SAISIR"))
                    ligne = ligne + premierFils.frereDroit.codeJava() + " = (new Scanner(System.in)).next();\n";
                else if(premierFils.valeur.equals("SAISIR_ENTIER"))
                    ligne = ligne + premierFils.frereDroit.codeJava() + " = (new Scanner(System.in)).nextInt();\n";
                else if(premierFils.valeur.equals("AFFICHER"))
                    ligne = ligne + "System.out.print("+ premierFils.frereDroit.codeJava()+");\n" ;
                else if(premierFils.valeur.equals("AFFICHER_LIGNE")){
                    ligne = ligne + "System.out.println("+ premierFils.frereDroit.codeJava()+");\n" ;
                }
                else if(premierFils.valeur.equals("RETOURNER"))
                    ligne = ligne + "return("+ premierFils.frereDroit.codeJava()+");\n" ;
            }
            return ligne;
        }
        else if(type.equals("<boucle pour>")){
            ligne = ligne + "for(" + premierFils.codeJava() + "=" + premierFils.frereDroit.codeJava() + ";";
            ligne = ligne + premierFils.codeJava() + "<=" + premierFils.frereDroit.frereDroit.codeJava() + ";";
            ligne = ligne + premierFils.codeJava() + "++) {\n";
            ligne = ligne + premierFils.frereDroit.frereDroit.frereDroit.codeJava() + "}\n";
            return ligne;
        }
        else if(type.equals("<condition>")){
            if(premierFils.frereDroit.frereDroit.frereDroit != null)
                ligne = ligne + "(";
    
            ligne = ligne + premierFils.codeJava();
            if(premierFils.frereDroit != null){
                if(premierFils.frereDroit.valeur.equals("="))
                    ligne = ligne + "==";
                else if (premierFils.frereDroit.valeur.equals("#"))
                    ligne = ligne + "!=";
                else if (premierFils.frereDroit.valeur.equals("<"))
                    ligne = ligne + "<";
                else if (premierFils.frereDroit.valeur.equals(">"))
                    ligne = ligne + ">";
            }
            ligne = ligne + premierFils.frereDroit.frereDroit.codeJava();

            if(premierFils.frereDroit.frereDroit.frereDroit != null){
                if(premierFils.frereDroit.frereDroit.frereDroit.valeur.equals("ET"))
                    ligne = ligne +" &&";
                else if(premierFils.frereDroit.frereDroit.frereDroit.valeur.equals("OU"))
                    ligne = ligne +" ||";

                ligne = ligne + premierFils.frereDroit.frereDroit.frereDroit.frereDroit.codeJava() + ")";
            }

        }
        else if(type.equals("<boucle tant que>")){
            ligne = ligne + "while(" + premierFils.codeJava() + "){\n";
            ligne = ligne + premierFils.frereDroit.codeJava() + "}\n";
        }
        else if((type.equals("<bloc si>"))){
            ligne = "if(" + premierFils.codeJava() + "){\n";
            ligne = ligne + premierFils.frereDroit.codeJava() + "}\n";
            if(premierFils.frereDroit.frereDroit!= null)
                ligne = ligne + "else{\n" + premierFils.frereDroit.frereDroit.codeJava() + "}\n";
        }
        else          // sert a retourner la ligne pour le type <feuille>
            return ligne;

        return ligne;
    }
    
    // sert a creer correctement le tableau de <liste instructions> contenant les instructions
    private String[] constructionListeInstructions(String valeur){
        String[] valeurs = valeur.split(" ");
        int nbSI = 0, nbFinSi = 0, nbPour = 0, nbFinPour = 0, nbTantQue = 0, nbFinTantQue = 0;

        // tableau pour retourner valeurs
        ArrayList<String> tab = new ArrayList<>();
        int i = 0;
        while (i < valeurs.length){
            if(!valeurs[i].isEmpty()){

                if(valeurs[i].equals("SI")){
                    int j = i;
                    String temp = "";
                    do{
                        temp += " "+valeurs[j];

                        if(valeurs[j].equals("SI"))
                            nbSI++;

                        if(valeurs[j].equals("FIN_SI"))
                            nbFinSi++;
                        j++;
                    }while(nbFinSi !=  nbSI);
                    i = j-1;
                    tab.add(temp);
                }

                else if(valeurs[i].equals("POUR")){
                    int j = i;
                    String temp = "";
                    do{
                        temp += valeurs[j]+" ";

                        if(valeurs[j].equals("POUR"))
                            nbPour++;

                        if(valeurs[j].equals("FIN_POUR"))
                            nbFinPour++;
                        j++;
                    }while(nbPour !=  nbFinPour);
                    i = j-1;
                    tab.add(temp);
                }

                else if(valeurs[i].equals("TANT_QUE")){
                    int j = i;
                    String temp = "";
                    do{
                        temp += valeurs[j]+" ";

                        if(valeurs[j].equals("TANT_QUE"))
                            nbTantQue++;

                        if(valeurs[j].equals("FIN_TANT_QUE"))
                            nbFinTantQue++;
                        j++;
                    }while(nbFinTantQue !=  nbTantQue);
                    i = j-1;
                    tab.add(temp);
                }

                else if(valeurs[i].contains(":=\"")){
                    int j = i;
                    String temp = valeurs[j];
                    do{
                        j++;
                        temp += " "+valeurs[j];
                    }while(!valeurs[j].contains("\""));
                    i = j;
                    tab.add(temp+" ");
                }
                else if(valeurs[i].contains(":="))
                    tab.add(valeurs[i]+" ");
                else if(valeurs[i].contains(":"))
                    tab.add(valeurs[i]+" ");
                else {
                    tab.add(valeurs[i]+" ");
                }
            }
            i++;
        }
        int tailleArrayList = tab.size();
        String[] tabFinal = new String[tailleArrayList];
        for(int indice = 0; indice <tailleArrayList; indice++)
            tabFinal[indice] = tab.get(indice);
        return tabFinal;
    }


}