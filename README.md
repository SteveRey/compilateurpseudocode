# Projet en Java au CNAM
## Création compilateur pseudo code vers du Java

Le projet consistait à "traduire" un pseudo code inventé (appelé NFP136 et illustré par les fichiers .nfp136) vers du langage Java, puis de créer le fichier .class associé.  
Le projet marche **entièrement** : il suffit de faire **java Main.java nomDuFichierATraduire** (sans mettre l'extension .nfp136).  
Le fichier **nomDuFichierATraduire** est ainsi traduit en Java et on peut l'exécuter en Java.  
Des fichiers d'exemple sont déjà fourni avec le code source.  
**Note importante :** il est nécéssaire que le nom du fichier .nfp136 à traduire corresponde au nom du programme définit à la première ligne du fichier .nfp136.  
En effet, Java impose que le nom du fichier soit le même que le nom de la classe.

Le fichier **ArbreBNF** contient la contruction de l'arbre BNF associé au langage NFP136.   
Cet arbre implémente donc la syntaxe du langage NFP136 qui suit la norme BNF définissant les langages.
