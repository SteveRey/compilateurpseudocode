import java.io.*;

/**
 * @param : il faut executer le programme de cette maniere :    java Main nomDuProgramme (sans l'extension)
 *
 * utilsation du programme :
 *  - mettre le nom du fichier a traduire sans l'extension ".nfp136"
 *  - le nom du fichier doit correspondre au nom du programme
 *      i.e. si dans le fichier .nfp136 le programme commence par : PROGRAMME CodeTest3
 *           alors il faut que le fichier s'appelle CodeTest3.nfp136
 *           car la traduction ne change pas le nom du fichier de sortie pour qu'il corresponde au nom du programme
 *           et Java requiert d'avoir un nom de fichier du meme nom que sa classe
 */

// la classe principale
class Main{
    public static void main(String[] args) {

        String MaClasse = args[0];


        try{

            // --------------- lecture du fichier passe en parametres --------------- //
            // -------- rappel : il faut le nom du fichier sans l'extention --------- //
            BufferedReader lecteur = new BufferedReader(new FileReader(MaClasse+".nfp136"));
            String ligne = lecteur.readLine();
            String contenu = new String(ligne);
            while(ligne != null){
                ligne = lecteur.readLine();
                if(ligne != null && !ligne.isEmpty())
                    contenu = contenu +" "+ligne;
            }

            // ----------  construction de l'abre BNF associe  ---------- //
            ArbreBNF arbre = new ArbreBNF(null, null, null, "<programme>", contenu);
            boolean isVrai = arbre.constructionArbreBNF();              // permet de detecter les erreurs de compilation (fonction recursive)
            if(!isVrai)                                                 // si erreur de compilation on avertit l'utilisateur
                System.out.println("Erreur de compilation");
            else {                                                      // sinon tout va bien et on continue
                String code = arbre.codeJava();                         // on "traduit" le langage nfp136 en Java (fonction recursive)
                try{
                    File file = new File(MaClasse+".java");
                    FileWriter fileWriter = new FileWriter(file);
                    fileWriter.write(code);
                    fileWriter.flush();
                    fileWriter.close();

                    Runtime r = Runtime.getRuntime();
                    Process p = r.exec("javac "+MaClasse+".java");
                    p.waitFor();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }


            lecteur.close();        // on ferme toujours le fichier quoi qu'il arrive
        }catch(Exception e){        // si le fichier ne peut pas etre lu on affiche une erreur
            e.printStackTrace();
        }
        
    }
}